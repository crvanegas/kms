﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class KeyTypeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbTipoLlave = New System.Windows.Forms.GroupBox()
        Me.rbTestPin = New System.Windows.Forms.RadioButton()
        Me.rbTestData = New System.Windows.Forms.RadioButton()
        Me.rbDatos = New System.Windows.Forms.RadioButton()
        Me.rbPIN = New System.Windows.Forms.RadioButton()
        Me.rbTransporte = New System.Windows.Forms.RadioButton()
        Me.rbMasterK = New System.Windows.Forms.RadioButton()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbComponentes = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.KeyNameTextBox = New System.Windows.Forms.TextBox()
        Me.gbTipoLlave.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbTipoLlave
        '
        Me.gbTipoLlave.Controls.Add(Me.rbTestPin)
        Me.gbTipoLlave.Controls.Add(Me.rbTestData)
        Me.gbTipoLlave.Controls.Add(Me.rbDatos)
        Me.gbTipoLlave.Controls.Add(Me.rbPIN)
        Me.gbTipoLlave.Controls.Add(Me.rbTransporte)
        Me.gbTipoLlave.Controls.Add(Me.rbMasterK)
        Me.gbTipoLlave.Location = New System.Drawing.Point(25, 44)
        Me.gbTipoLlave.Name = "gbTipoLlave"
        Me.gbTipoLlave.Size = New System.Drawing.Size(216, 133)
        Me.gbTipoLlave.TabIndex = 0
        Me.gbTipoLlave.TabStop = False
        Me.gbTipoLlave.Text = "Tipo Llave"
        '
        'rbTestPin
        '
        Me.rbTestPin.AutoSize = True
        Me.rbTestPin.Location = New System.Drawing.Point(117, 28)
        Me.rbTestPin.Name = "rbTestPin"
        Me.rbTestPin.Size = New System.Drawing.Size(80, 17)
        Me.rbTestPin.TabIndex = 5
        Me.rbTestPin.TabStop = True
        Me.rbTestPin.Text = "Prueba PIN"
        Me.rbTestPin.UseVisualStyleBackColor = True
        '
        'rbTestData
        '
        Me.rbTestData.AutoSize = True
        Me.rbTestData.Location = New System.Drawing.Point(117, 51)
        Me.rbTestData.Name = "rbTestData"
        Me.rbTestData.Size = New System.Drawing.Size(90, 17)
        Me.rbTestData.TabIndex = 4
        Me.rbTestData.TabStop = True
        Me.rbTestData.Text = "Prueba Datos"
        Me.rbTestData.UseVisualStyleBackColor = True
        '
        'rbDatos
        '
        Me.rbDatos.AutoSize = True
        Me.rbDatos.Location = New System.Drawing.Point(22, 97)
        Me.rbDatos.Name = "rbDatos"
        Me.rbDatos.Size = New System.Drawing.Size(53, 17)
        Me.rbDatos.TabIndex = 3
        Me.rbDatos.TabStop = True
        Me.rbDatos.Text = "Datos"
        Me.rbDatos.UseVisualStyleBackColor = True
        '
        'rbPIN
        '
        Me.rbPIN.AutoSize = True
        Me.rbPIN.Location = New System.Drawing.Point(22, 74)
        Me.rbPIN.Name = "rbPIN"
        Me.rbPIN.Size = New System.Drawing.Size(43, 17)
        Me.rbPIN.TabIndex = 2
        Me.rbPIN.TabStop = True
        Me.rbPIN.Text = "PIN"
        Me.rbPIN.UseVisualStyleBackColor = True
        '
        'rbTransporte
        '
        Me.rbTransporte.AutoSize = True
        Me.rbTransporte.Location = New System.Drawing.Point(22, 51)
        Me.rbTransporte.Name = "rbTransporte"
        Me.rbTransporte.Size = New System.Drawing.Size(76, 17)
        Me.rbTransporte.TabIndex = 1
        Me.rbTransporte.TabStop = True
        Me.rbTransporte.Text = "Transporte"
        Me.rbTransporte.UseVisualStyleBackColor = True
        '
        'rbMasterK
        '
        Me.rbMasterK.AutoSize = True
        Me.rbMasterK.Location = New System.Drawing.Point(22, 28)
        Me.rbMasterK.Name = "rbMasterK"
        Me.rbMasterK.Size = New System.Drawing.Size(63, 17)
        Me.rbMasterK.TabIndex = 0
        Me.rbMasterK.TabStop = True
        Me.rbMasterK.Text = "Maestra"
        Me.rbMasterK.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Enabled = False
        Me.btnNext.Location = New System.Drawing.Point(166, 224)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "Siguiente"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 187)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Número de componentes"
        '
        'cmbComponentes
        '
        Me.cmbComponentes.FormattingEnabled = True
        Me.cmbComponentes.Items.AddRange(New Object() {"2", "3"})
        Me.cmbComponentes.Location = New System.Drawing.Point(185, 184)
        Me.cmbComponentes.Name = "cmbComponentes"
        Me.cmbComponentes.Size = New System.Drawing.Size(56, 21)
        Me.cmbComponentes.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nombre llave"
        '
        'KeyNameTextBox
        '
        Me.KeyNameTextBox.Location = New System.Drawing.Point(94, 10)
        Me.KeyNameTextBox.Name = "KeyNameTextBox"
        Me.KeyNameTextBox.Size = New System.Drawing.Size(147, 20)
        Me.KeyNameTextBox.TabIndex = 5
        '
        'KeyTypeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 252)
        Me.Controls.Add(Me.KeyNameTextBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbComponentes)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.gbTipoLlave)
        Me.Name = "KeyTypeForm"
        Me.Text = "Configuración de llave"
        Me.gbTipoLlave.ResumeLayout(False)
        Me.gbTipoLlave.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbTipoLlave As System.Windows.Forms.GroupBox
    Friend WithEvents rbDatos As System.Windows.Forms.RadioButton
    Friend WithEvents rbPIN As System.Windows.Forms.RadioButton
    Friend WithEvents rbTransporte As System.Windows.Forms.RadioButton
    Friend WithEvents rbMasterK As System.Windows.Forms.RadioButton
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbComponentes As System.Windows.Forms.ComboBox
    Friend WithEvents rbTestPin As System.Windows.Forms.RadioButton
    Friend WithEvents rbTestData As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents KeyNameTextBox As System.Windows.Forms.TextBox
End Class
