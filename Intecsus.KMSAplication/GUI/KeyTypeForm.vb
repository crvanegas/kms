﻿Imports System.Text
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Environment
Public Class KeyTypeForm

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        LoadValues()
    End Sub

    Private Sub LoadValues()
        Dim res As Integer
        Dim buffer_sb As StringBuilder
        buffer_sb = New StringBuilder(512)

        res = GetPrivateProfileString("key", "key_name", "", buffer_sb, buffer_sb.Capacity, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)
        KeyNameTextBox.Text = buffer_sb.ToString()

    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        Dim res As Integer
        Dim LoadKeyForm As LoadKeyForm
        Dim vcomponente As Integer
        If Integer.TryParse(cmbComponentes.SelectedItem.ToString(), vcomponente) Then
        End If

        If KeyNameTextBox.Text = "" Then
            MsgBox("Nombre de la llave no puede ser vacío")

            Return
        End If

        Dim strNom As String = String.Empty
        Dim nombre As String = ComprobarNombre(KeyNameTextBox.Text.ToString())

        If rbDatos.Checked Then
            strNom = TipoLlave.TipoLlave.Datos + "." + nombre.Trim()
        End If

        If rbMasterK.Checked Then
            strNom = TipoLlave.TipoLlave.Maestra + "." + nombre.Trim()
           
        End If

        If rbPIN.Checked Then
            strNom = TipoLlave.TipoLlave.PIN + "." + nombre.Trim()
          
        End If

        If rbTransporte.Checked Then
            strNom = TipoLlave.TipoLlave.Transporte + "." + nombre.Trim()
          
        End If


        If rbTestData.Checked Then
            strNom = TipoLlave.TipoLlave.PruebaDatos + "." + nombre.Trim()
          
        End If

        If rbTestPin.Checked Then
            strNom = TipoLlave.TipoLlave.PruebaPIN + "." + nombre.Trim()
            
        End If

        res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)



        LoadKeyForm = New LoadKeyForm(vcomponente)
        LoadKeyForm.StartPosition = FormStartPosition.CenterParent
        LoadKeyForm.ShowDialog(Me)
    End Sub


    Private Sub radioButtons_CheckedChanged(sender As Object, e As EventArgs) Handles rbMasterK.CheckedChanged, rbTransporte.CheckedChanged, rbPIN.CheckedChanged, rbDatos.CheckedChanged, rbTestPin.CheckedChanged, rbTestData.CheckedChanged

        'Se comprueba el nombre de la lave y se asigna la etiqueta para el nombre
        Dim res As Integer
        Dim buffer_sb As StringBuilder
        buffer_sb = New StringBuilder(512)


        If rbDatos.Checked Or rbMasterK.Checked Or rbPIN.Checked Or rbTransporte.Checked Or rbTestData.Checked Or rbTestPin.Checked Then
            Dim strNom As String = String.Empty
            res = GetPrivateProfileString("key", "key_name", "", buffer_sb, buffer_sb.Capacity, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)

            Dim nombre As String = ComprobarNombre(buffer_sb.ToString())

            KeyNameTextBox.Text = nombre

            If rbDatos.Checked Then
                strNom = TipoLlave.TipoLlave.Datos + "." + nombre.Trim()
                res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)
            End If

            If rbMasterK.Checked Then
                strNom = TipoLlave.TipoLlave.Maestra + "." + nombre.Trim()
                res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)

            End If

            If rbPIN.Checked Then
                strNom = TipoLlave.TipoLlave.PIN + "." + nombre.Trim()
                res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)

            End If

            If rbTransporte.Checked Then
                strNom = TipoLlave.TipoLlave.Transporte + "." + nombre.Trim()
                res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)

            End If


            If rbTestData.Checked Then
                strNom = TipoLlave.TipoLlave.PruebaDatos + "." + nombre.Trim()
                res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)

            End If

            If rbTestPin.Checked Then
                strNom = TipoLlave.TipoLlave.PruebaPIN + "." + nombre.Trim()
                res = WritePrivateProfileString("key", "key_name", strNom, GetFolderPath(SpecialFolder.ApplicationData) & CONFIG_PATH)

            End If

            KeyNameTextBox.Text = strNom
        End If

        If cmbComponentes.SelectedItem <> 0 Then
            btnNext.Enabled = True
        End If


    End Sub
    ''' <summary>
    ''' Función para comprobar el nombre de la llave si posee alguna nomenclatura
    ''' </summary>
    ''' <param name="strNombre">Nombre que se desea comprobar</param>
    ''' <returns>Nombre post comprobación</returns>
    ''' <remarks></remarks>
    Public Function ComprobarNombre(strNombre As String) As String

        ' Split the string on the backslash character.
        Dim parts As String() = strNombre.Split(New Char() {"."c})

        ' Loop through result strings with For Each.

        Dim part As String = parts(0)

        Select Case part
            Case TipoLlave.TipoLlave.PIN, TipoLlave.TipoLlave.Datos, TipoLlave.TipoLlave.Maestra, TipoLlave.TipoLlave.Transporte, TipoLlave.TipoLlave.PruebaDatos, TipoLlave.TipoLlave.PruebaPIN
                Dim list As New List(Of String)(parts)
                list.RemoveAt(0)
                parts = list.ToArray
                strNombre = String.Join(".", parts)
                Return strNombre
            Case Else
                Return strNombre
        End Select


    End Function


    Private Const CONFIG_PATH As String = "\IntecsusKMS\config.ini"

    Private Declare Auto Function GetPrivateProfileString Lib "kernel32" (ByVal lpSectionName As String,
        ByVal lpKeyName As String,
        ByVal lpDefault As String,
        ByVal lpReturnedString As StringBuilder,
        ByVal nSize As Integer,
        ByVal lpFileName As String) As Integer

    Private Declare Auto Function WritePrivateProfileString Lib "kernel32" (ByVal lpSectionName As String,
       ByVal lpKeyName As String,
       ByVal lpString As String,
       ByVal lpFileName As String) As Integer



    Private Sub cmbComponentes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbComponentes.SelectedIndexChanged
        If cmbComponentes.SelectedItem <> 0 Then
            If rbDatos.Checked Or rbMasterK.Checked Or rbPIN.Checked Or rbTransporte.Checked Or rbTestData.Checked Or rbTestPin.Checked Then
                btnNext.Enabled = True
            End If
        End If
    End Sub
End Class