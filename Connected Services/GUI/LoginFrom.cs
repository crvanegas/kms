﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using IntecsusKMS.Entidad;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using KMS.Interfaces;
using KMS.Implementacion;

namespace IntecsusKMS
{
    public partial class LoginFrom : Form
    {
        #region "Implentacion LoginFrom"

        public LoginFrom()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            Logger.SetLogWriter(new LogWriterFactory().Create());
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            try
            {
                Componentes Comp = new Componentes();
                ILogin KMS = new LoginKMS();
                if (KMS.Login(UserTextBox.Text, PasswordTextBox.Text, Comp.RutaArchivo, "login", "user", "pw", 512))
                {  
                    this.Hide();
                    Form form = new MainAppForm();
                    form.StartPosition = FormStartPosition.CenterScreen;
                    form.Visible = true;
                    this.Activate();
                }
                else
                {
                    Logger.Write("Error al validar usuario", "ExceptionHandling");
                    UserTextBox.Text = string.Empty;
                    PasswordTextBox.Text = string.Empty;
                    throw new Exception("Error al validar credenciales");
                }
            }
            catch (Exception ex)
            {
                Logger.Write("Ocurrió un error. Descripción:" + ex.Message, "ExceptionHandling");
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        #endregion

        private void LoginFrom_FormClosing(object sender, FormClosingEventArgs e)
        {
            switch (e.CloseReason)
            {
                case CloseReason.UserClosing:
                    if (MessageBox.Show("Seguro que quieres salir ? ",
                                        "Salir?",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question) == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                    break;
            }
        }
    }
}
