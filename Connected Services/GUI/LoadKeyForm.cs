﻿using Intecsus.Criptografia.Implementacion;
using Intecsus.Criptografia.Interface;
using Intecsus.Utilidades;
using IntecsusKMS.Entidad;
using KMS.Implementacion;
using KMS.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace IntecsusKMS
{    public partial class LoadKeyForm : Form
    {
        Componentes Comp;
        ICriptografia m_cripto = new Cripto();
        Operaciones Ope = new Operaciones();

        public LoadKeyForm(Componentes C)
        {
            InitializeComponent();
            Comp = C;
            Comp.Componente.Clear();
            C2Button.Visible = false;
            C3Button.Visible = false;
            LoadKeyButton.Visible = false;
        }
        private void C1Button_Click(object sender, EventArgs e)
        {
            CambiarTitulo("Cargar Componente 1", C1Button);
            C2Button.Visible = true;
            C2Button.Enabled = true;
        }

        private void C2Button_Click(object sender, EventArgs e)
        {
            CambiarTitulo("Cargar Componente 2", C2Button);

            if (Comp.CantidadComponentes == "2")
            {
                LoadKeyButton.Visible = true;
                LoadKeyButton.Enabled = true;
            }
            else
            {
                C3Button.Visible = true;
                C3Button.Enabled = true;
            }
        }

        private void C3Button_Click(object sender, EventArgs e)
        {
            CambiarTitulo("Cargar Componente 3", C3Button);
            LoadKeyButton.Visible = true;
            LoadKeyButton.Enabled = true;
        }

        private void CambiarTitulo(string Titulo, Button Boton)
        {
            LoadComponentForm LoadCForm = new LoadComponentForm(Titulo, Comp);
            LoadCForm.StartPosition = FormStartPosition.CenterScreen;
            LoadCForm.Visible = true;
            Boton.Visible = true;
            Boton.Enabled = true;
        }

        private void LoadKeyButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Comp.Componente == null || Comp.Componente.Count != (Convert.ToInt32(Comp.CantidadComponentes)))
                {
                    Comp.Componente.Clear();
                    MessageBox.Show("Cargue nuevamente todos los componentes de la llave \n" +
                        "Antes de seleccionar cargar la Llave", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    ICargarLLave Cargar = new CargarLlave();
                    ServicioKMS.KMSClient OperarServicio = new ServicioKMS.KMSClient();
                    string Key = string.Empty;
                    if (Comp.TipoLlave == "DES")
                    {
                        Key = Cargar.GenerarLLave(Comp.Componente, false);
                        Key = Key + Key;
                    }
                    else
                    { Key = Cargar.GenerarLLave(Comp.Componente, true); }
                    string resp = string.Empty;
                    ICriptografia m_cripto = new Cripto();
                    Comp.Llave = m_cripto.Base64Encode(Key);
                    if (Comp.CargarEn == "PINOFFST")
                    {
                        this.Hide();
                        PINOffSet PSET = new PINOffSet(Comp,OperarServicio);
                        PSET.StartPosition = FormStartPosition.CenterScreen;
                        PSET.Visible = true;
                    }
                    else if (Comp.CargarEn == "MASTER")
                    {                      
                        resp = OperarServicio.AlmacenarMK(Comp.Llave);
                        Ope.BaseDatos(resp);
                    }
                    else if (Comp.CargarEn == "SESION")
                    {
                        resp = OperarServicio.AlmacenarSK(Comp.Llave);
                        Ope.BaseDatos(resp);                        
                    }
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                this.Close();
                VisorEventos ViEv = new VisorEventos();
                ViEv.GuardarLog("Application", "Intecsus.BD", "EjecutarSP " + ex.Message, EventLogEntryType.Error);

                Logger.Write("Ocurrió un error. Descripción:" + ex.Message + EventLogEntryType.Error, "ExceptionHandling");
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
        
