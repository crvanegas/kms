﻿namespace IntecsusKMS
{
    partial class MainAppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainAppForm));
            this.OperacionesGroupBox = new System.Windows.Forms.GroupBox();
            this.LoadKeyButton = new System.Windows.Forms.Button();
            this.GenKeyButton = new System.Windows.Forms.Button();
            this.MenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.OpcionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ConfiguraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AyudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AcercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OperacionesGroupBox.SuspendLayout();
            this.MenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OperacionesGroupBox
            // 
            resources.ApplyResources(this.OperacionesGroupBox, "OperacionesGroupBox");
            this.OperacionesGroupBox.Controls.Add(this.LoadKeyButton);
            this.OperacionesGroupBox.Controls.Add(this.GenKeyButton);
            this.OperacionesGroupBox.Name = "OperacionesGroupBox";
            this.OperacionesGroupBox.TabStop = false;
            // 
            // LoadKeyButton
            // 
            resources.ApplyResources(this.LoadKeyButton, "LoadKeyButton");
            this.LoadKeyButton.Name = "LoadKeyButton";
            this.LoadKeyButton.UseVisualStyleBackColor = true;
            this.LoadKeyButton.Click += new System.EventHandler(this.LoadKeyButton_Click);
            // 
            // GenKeyButton
            // 
            resources.ApplyResources(this.GenKeyButton, "GenKeyButton");
            this.GenKeyButton.Name = "GenKeyButton";
            this.GenKeyButton.UseVisualStyleBackColor = true;
            // 
            // MenuStrip1
            // 
            resources.ApplyResources(this.MenuStrip1, "MenuStrip1");
            this.MenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpcionesToolStripMenuItem,
            this.AyudaToolStripMenuItem});
            this.MenuStrip1.Name = "MenuStrip1";
            // 
            // OpcionesToolStripMenuItem
            // 
            resources.ApplyResources(this.OpcionesToolStripMenuItem, "OpcionesToolStripMenuItem");
            this.OpcionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ConfiguraciónToolStripMenuItem});
            this.OpcionesToolStripMenuItem.Name = "OpcionesToolStripMenuItem";
            // 
            // ConfiguraciónToolStripMenuItem
            // 
            resources.ApplyResources(this.ConfiguraciónToolStripMenuItem, "ConfiguraciónToolStripMenuItem");
            this.ConfiguraciónToolStripMenuItem.Name = "ConfiguraciónToolStripMenuItem";
            this.ConfiguraciónToolStripMenuItem.Click += new System.EventHandler(this.ConfiguraciónToolStripMenuItem_Click);
            // 
            // AyudaToolStripMenuItem
            // 
            resources.ApplyResources(this.AyudaToolStripMenuItem, "AyudaToolStripMenuItem");
            this.AyudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AcercaDeToolStripMenuItem});
            this.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem";
            // 
            // AcercaDeToolStripMenuItem
            // 
            resources.ApplyResources(this.AcercaDeToolStripMenuItem, "AcercaDeToolStripMenuItem");
            this.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem";
            this.AcercaDeToolStripMenuItem.Click += new System.EventHandler(this.AcercaDeToolStripMenuItem_Click);
            // 
            // MainAppForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.OperacionesGroupBox);
            this.Controls.Add(this.MenuStrip1);
            this.Name = "MainAppForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainAppForm_FormClosing);
            this.OperacionesGroupBox.ResumeLayout(false);
            this.MenuStrip1.ResumeLayout(false);
            this.MenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox OperacionesGroupBox;
        internal System.Windows.Forms.Button LoadKeyButton;
        internal System.Windows.Forms.Button GenKeyButton;
        internal System.Windows.Forms.MenuStrip MenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem OpcionesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ConfiguraciónToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AyudaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AcercaDeToolStripMenuItem;
    }
}