﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Intecsus.Criptografia.Implementacion;
using Intecsus.Criptografia.Interface;
using IntecsusKMS.Entidad;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace IntecsusKMS
{

    public partial class LoadComponentForm : Form
    {
        Componentes Comp;
        Operaciones Ope = new Operaciones();
        ICriptografia m_Crip = new Cripto();
        Int32 TLlave = 0;
        Boolean TDES = true;
        public LoadComponentForm(string Title, Componentes C)
        {
            Comp = C;
            InitializeComponent();           
            this.Text = Title;
            LoadComponentButton.Enabled = false;
            if (Comp.TipoLlave == "TDES")
            {
                TLlave = 32;
                TDES = true;
            }
            else
            {
                this.ClientSize = new System.Drawing.Size(250, 173);
                this.GroupBox1.Size = new System.Drawing.Size(220, 119);
                this.ComponentTextBox.MaxLength = 16;
                this.ComponentTextBox.Size = new System.Drawing.Size(120, 20);
                TLlave = 16;
                TDES = false;
            }
        }

        private void ComponentTextBox_TextChanged(object sender, EventArgs e)
        {
            TextChanged();
        }

        /// <summary>
        /// Metodo encargados de generar la implentacion de Carga de Componentes 
        /// </summary>
        private new void TextChanged()
        {
            try
            {
                FormatLabel.Text = Ope.VerifiCaracteres(ComponentTextBox.Text, "0-9,A-F");
              if (FormatLabel.Text == string.Empty && (ComponentTextBox.Text.Length) == TLlave)
                {
                        KCVTextBox.Text = m_Crip.KCV(ComponentTextBox.Text, TDES);
                }
                else
                {
                    KCVTextBox.Text = string.Empty;
                }
                if (FormatLabel.Text == string.Empty && KCVTextBox.Text != string.Empty)
                {
                    LoadComponentButton.Enabled = true;
                }
                else
                {
                    LoadComponentButton.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "La clave especificada es una clave débil conocida para 'TripleDES' y no se puede utilizar.")
                {
                    DialogResult dialogResult = MessageBox.Show("La clave especificada es débil." + "\n ¿Esta seguro de usarla?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialogResult == DialogResult.Yes)
                    {
                        KCVTextBox.Text = m_Crip.KCV(ComponentTextBox.Text.Substring(0,16), false);
                        if (FormatLabel.Text == string.Empty && KCVTextBox.Text != string.Empty)
                        {
                            LoadComponentButton.Enabled = true;
                        }
                        else
                        {
                            LoadComponentButton.Enabled = false;
                        }
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        ComponentTextBox.Text = string.Empty;
                        Logger.Write("Ocurrió un error. Descripción:" + ex.Message, "ExceptionHandling");
                        MessageBox.Show("Escriba Nuevamente la llave", "", MessageBoxButtons.OK, MessageBoxIcon.Information);                       
                    }
                }
                else
                {
                    MessageBox.Show("Escriba Nuevamente la llave" + ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    throw new Exception(ex.Message);
                }
            }
        }

        private void LoadComponentButton_Click(object sender, EventArgs e)
        {
            Comp.Componente.Add(ComponentTextBox.Text);
            this.KCVTextBox.Text = string.Empty;
            this.ComponentTextBox.Text = string.Empty;
            this.Hide();
        }

        private void LoadComponentForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
