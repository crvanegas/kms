﻿using System;
using IntecsusKMS.Entidad;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Intecsus.Criptografia.Interface;
using Intecsus.Criptografia.Implementacion;

namespace IntecsusKMS
{
    public partial class PINOffSet : Form
    {
        Operaciones Ope = new Operaciones();
        Componentes Comp;
        ServicioKMS.KMSClient OperarServicio;
        public PINOffSet(Componentes C,ServicioKMS.KMSClient OpServ)
        {
            Comp = C;
            OperarServicio = OpServ;
            InitializeComponent();
            btnCargar.Enabled = false;
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                ServicioKMS.KMSClient OperarServicio = new ServicioKMS.KMSClient();
                string resp = OperarServicio.AlmacenarVariablesPINOffSet(Comp.Llave, Convert.ToInt32(SelcInicio.Text), Convert.ToInt32(SelcLong.Text), SelcPadeo.Text, Convert.ToInt32(SelcLongPin.Text), TBoxTabDec.Text);
                Ope.BaseDatos(resp);
                this.Close();
            }
            catch (Exception ex)
            {
                this.Close();
                Logger.Write("Ocurrió un error. Descripción:" + "Error conexión Base de Datos" + ex.Message, "ExceptionHandling");
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void TBoxTabDec_TextChanged_1(object sender, EventArgs e)
        {
            LabelForm.Text = Ope.VerifiCaracteres(TBoxTabDec.Text, "0-9");
            if (SelcInicio.Text != string.Empty &&
                SelcLong.Text != string.Empty &&
                SelcPadeo.Text != string.Empty &&
                SelcLongPin.Text != string.Empty &&
                LabelForm.Text == string.Empty &&
                TBoxTabDec.Text.Length == 16)
            {
                btnCargar.Enabled = true;
            }
            else
            {
                btnCargar.Enabled = false;
            }
        }

    }
}
