﻿using IntecsusKMS.Entidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntecsusKMS
{
    public partial class MainAppForm : Form
    {
        Componentes Comp = new Componentes();
        public MainAppForm()
        {          
            InitializeComponent();
        }

        private void LoadKeyButton_Click(object sender, EventArgs e)
        {         
            KeyTypeForm KeyType = new KeyTypeForm(Comp);
            KeyType.StartPosition = FormStartPosition.CenterScreen;
            KeyType.Visible = true;
        }

        private void MainAppForm_FormClosing(object sender, FormClosingEventArgs e)
        {
         // if (MessageBox.Show("¿Desea salir de la Aplicacion?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
            {
                Application.Exit(); // or this.Close();
            }
        }

        private void AcercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm About = new AboutForm();
            About.StartPosition = FormStartPosition.CenterScreen;
            About.Visible = true;
        }

        private void ConfiguraciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserConfForm ConfUser = new UserConfForm();
            ConfUser.StartPosition = FormStartPosition.CenterScreen;
            ConfUser.Visible = true;
        }
    }
}
