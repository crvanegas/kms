﻿namespace IntecsusKMS
{
    partial class UserConfForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserConfForm));
            this.SaveButton = new System.Windows.Forms.Button();
            this.UserGroupBox = new System.Windows.Forms.GroupBox();
            this.PwTextBox = new System.Windows.Forms.TextBox();
            this.PwLabel = new System.Windows.Forms.Label();
            this.Pw2TextBox = new System.Windows.Forms.TextBox();
            this.Pw1TextBox = new System.Windows.Forms.TextBox();
            this.UserTextBox = new System.Windows.Forms.TextBox();
            this.Pw2Label = new System.Windows.Forms.Label();
            this.Pw1Label = new System.Windows.Forms.Label();
            this.UserLabel = new System.Windows.Forms.Label();
            this.UserGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(225, 161);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(113, 26);
            this.SaveButton.TabIndex = 18;
            this.SaveButton.Text = "Guardar";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // UserGroupBox
            // 
            this.UserGroupBox.Controls.Add(this.PwTextBox);
            this.UserGroupBox.Controls.Add(this.PwLabel);
            this.UserGroupBox.Controls.Add(this.Pw2TextBox);
            this.UserGroupBox.Controls.Add(this.Pw1TextBox);
            this.UserGroupBox.Controls.Add(this.UserTextBox);
            this.UserGroupBox.Controls.Add(this.Pw2Label);
            this.UserGroupBox.Controls.Add(this.Pw1Label);
            this.UserGroupBox.Controls.Add(this.UserLabel);
            this.UserGroupBox.Location = new System.Drawing.Point(12, 12);
            this.UserGroupBox.Name = "UserGroupBox";
            this.UserGroupBox.Size = new System.Drawing.Size(326, 143);
            this.UserGroupBox.TabIndex = 17;
            this.UserGroupBox.TabStop = false;
            this.UserGroupBox.Text = "Informacion de usuario";
            // 
            // PwTextBox
            // 
            this.PwTextBox.Location = new System.Drawing.Point(128, 60);
            this.PwTextBox.Name = "PwTextBox";
            this.PwTextBox.PasswordChar = '•';
            this.PwTextBox.Size = new System.Drawing.Size(192, 20);
            this.PwTextBox.TabIndex = 4;
            // 
            // PwLabel
            // 
            this.PwLabel.AutoSize = true;
            this.PwLabel.Location = new System.Drawing.Point(30, 60);
            this.PwLabel.Name = "PwLabel";
            this.PwLabel.Size = new System.Drawing.Size(88, 13);
            this.PwLabel.TabIndex = 3;
            this.PwLabel.Text = "Password actual:";
            // 
            // Pw2TextBox
            // 
            this.Pw2TextBox.Location = new System.Drawing.Point(128, 112);
            this.Pw2TextBox.Name = "Pw2TextBox";
            this.Pw2TextBox.PasswordChar = '•';
            this.Pw2TextBox.Size = new System.Drawing.Size(192, 20);
            this.Pw2TextBox.TabIndex = 8;
            // 
            // Pw1TextBox
            // 
            this.Pw1TextBox.Location = new System.Drawing.Point(128, 86);
            this.Pw1TextBox.Name = "Pw1TextBox";
            this.Pw1TextBox.PasswordChar = '•';
            this.Pw1TextBox.Size = new System.Drawing.Size(192, 20);
            this.Pw1TextBox.TabIndex = 6;
            // 
            // UserTextBox
            // 
            this.UserTextBox.Location = new System.Drawing.Point(128, 30);
            this.UserTextBox.Name = "UserTextBox";
            this.UserTextBox.Size = new System.Drawing.Size(192, 20);
            this.UserTextBox.TabIndex = 2;
            // 
            // Pw2Label
            // 
            this.Pw2Label.AutoSize = true;
            this.Pw2Label.Location = new System.Drawing.Point(16, 115);
            this.Pw2Label.Name = "Pw2Label";
            this.Pw2Label.Size = new System.Drawing.Size(102, 13);
            this.Pw2Label.TabIndex = 7;
            this.Pw2Label.Text = "Confirmar password:";
            // 
            // Pw1Label
            // 
            this.Pw1Label.AutoSize = true;
            this.Pw1Label.Location = new System.Drawing.Point(29, 89);
            this.Pw1Label.Name = "Pw1Label";
            this.Pw1Label.Size = new System.Drawing.Size(89, 13);
            this.Pw1Label.TabIndex = 5;
            this.Pw1Label.Text = "Password nuevo:";
            // 
            // UserLabel
            // 
            this.UserLabel.AutoSize = true;
            this.UserLabel.Location = new System.Drawing.Point(72, 33);
            this.UserLabel.Name = "UserLabel";
            this.UserLabel.Size = new System.Drawing.Size(46, 13);
            this.UserLabel.TabIndex = 1;
            this.UserLabel.Text = "Usuario:";
            // 
            // UserConfForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 197);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.UserGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserConfForm";
            this.Text = "Configuracion de usuario";
            this.UserGroupBox.ResumeLayout(false);
            this.UserGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button SaveButton;
        internal System.Windows.Forms.GroupBox UserGroupBox;
        internal System.Windows.Forms.TextBox PwTextBox;
        internal System.Windows.Forms.Label PwLabel;
        internal System.Windows.Forms.TextBox Pw2TextBox;
        internal System.Windows.Forms.TextBox Pw1TextBox;
        internal System.Windows.Forms.TextBox UserTextBox;
        internal System.Windows.Forms.Label Pw2Label;
        internal System.Windows.Forms.Label Pw1Label;
        internal System.Windows.Forms.Label UserLabel;
    }
}