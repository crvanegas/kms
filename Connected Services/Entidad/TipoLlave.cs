﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntecsusKMS.Entidad
{
    public class TipoLlave
    {
        public Dictionary<string, string> TLlave = new Dictionary<string, string>() {
            //Tipos de LLaves a almacenar
            { "rbMasterK", "MASTER"},
            { "rbTransporte", "SESION"},
            { "rbPIN", "PIN"},
            { "rbDatos", "DATA"},
            { "rbTestPin", "TPIN"},
            { "rbTestData", "TDATA"},
            { "rbPOffSet", "PINOFFST"},            
    };}
    }
