﻿Imports System.Text.RegularExpressions

Public Class LoadComponentForm
    Private Component As Component
    Private EsUltimo As Boolean

    Public Sub New(Title As String, Component As Component)
        MyBase.New()
        InitializeComponent()
        'EsUltimo = Ultimo
        Me.Text = Title
        Me.Component = Component
        LoadComponentButton.Enabled = False
    End Sub

    Private Sub LoadComponentButton_Click(sender As Object, e As EventArgs) Handles LoadComponentButton.Click
        Me.Component.setIsValid(True)
        Me.Component.setComponent(ComponentTextBoxL.Text + ComponentTextBoxR.Text)

        MainAppForm.Enabled = True
        Me.Dispose()
    End Sub
    ''' <summary>
    ''' Evento onchange del los input textbox los cuales verifica que sean de tamaño 16 y calculan el kcv
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ComponentTextBox_OnChange(sender As Object, e As EventArgs) Handles ComponentTextBoxL.TextChanged
        If Not Regex.Match(ComponentTextBoxL.Text, "^[0-9,A-F]*$", RegexOptions.IgnoreCase).Success Then
            FormatLabel.Text = "Usar sólo caracteres hexadecimal: 0-9, A-F"
        Else
            FormatLabel.Text = ""
            If Len(ComponentTextBoxL.Text + ComponentTextBoxR.Text) = 32 Then
                Dim tdes As TDES
                tdes = New TDES()
                KCVTextBox.Text = UCase(tdes.DigitoChequeo(6, ComponentTextBoxL.Text + ComponentTextBoxR.Text))
                LoadComponentButton.Enabled = True
            Else
                KCVTextBox.Text = ""
                LoadComponentButton.Enabled = False
            End If
        End If
    End Sub
    Private Sub ComponentTextBoxR_OnChange(sender As Object, e As EventArgs) Handles ComponentTextBoxR.TextChanged
        If Not Regex.Match(ComponentTextBoxR.Text, "^[0-9,A-F]*$", RegexOptions.IgnoreCase).Success Then
            FormatLabel.Text = "Usar sólo caracteres hexadecimal: 0-9, A-F"
        Else
            FormatLabel.Text = ""
            If Len(ComponentTextBoxL.Text + ComponentTextBoxR.Text) = 32 Then
                Dim tdes As TDES
                tdes = New TDES()
                KCVTextBox.Text = UCase(tdes.DigitoChequeo(6, ComponentTextBoxL.Text + ComponentTextBoxR.Text))
                LoadComponentButton.Enabled = True
            Else
                KCVTextBox.Text = ""
                LoadComponentButton.Enabled = False
            End If
        End If
    End Sub

    Private Sub LoadComponentForm_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        MainAppForm.Enabled = True
    End Sub

    Private Sub KCVTextBox_TextChanged(sender As Object, e As EventArgs) Handles KCVTextBox.TextChanged

    End Sub

    Private Sub LoadComponentForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub


    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub
End Class