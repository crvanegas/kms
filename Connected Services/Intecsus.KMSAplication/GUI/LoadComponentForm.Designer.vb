﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoadComponentForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoadComponentForm))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComponentTextBoxR = New System.Windows.Forms.TextBox()
        Me.FormatLabel = New System.Windows.Forms.Label()
        Me.KCVTextBox = New System.Windows.Forms.TextBox()
        Me.KCVLabel = New System.Windows.Forms.Label()
        Me.ComponentTextBoxL = New System.Windows.Forms.TextBox()
        Me.ComponentLabel = New System.Windows.Forms.Label()
        Me.LoadComponentButton = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ComponentTextBoxR)
        Me.GroupBox1.Controls.Add(Me.FormatLabel)
        Me.GroupBox1.Controls.Add(Me.KCVTextBox)
        Me.GroupBox1.Controls.Add(Me.KCVLabel)
        Me.GroupBox1.Controls.Add(Me.ComponentTextBoxL)
        Me.GroupBox1.Controls.Add(Me.ComponentLabel)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 23)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(349, 149)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Componente"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(258, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Derecha"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(120, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Izquierda"
        '
        'ComponentTextBoxR
        '
        Me.ComponentTextBoxR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ComponentTextBoxR.Location = New System.Drawing.Point(217, 41)
        Me.ComponentTextBoxR.MaxLength = 16
        Me.ComponentTextBoxR.Name = "ComponentTextBoxR"
        Me.ComponentTextBoxR.Size = New System.Drawing.Size(130, 20)
        Me.ComponentTextBoxR.TabIndex = 6
        '
        'FormatLabel
        '
        Me.FormatLabel.AutoSize = True
        Me.FormatLabel.ForeColor = System.Drawing.Color.Red
        Me.FormatLabel.Location = New System.Drawing.Point(79, 87)
        Me.FormatLabel.Name = "FormatLabel"
        Me.FormatLabel.Size = New System.Drawing.Size(0, 13)
        Me.FormatLabel.TabIndex = 5
        '
        'KCVTextBox
        '
        Me.KCVTextBox.Location = New System.Drawing.Point(81, 114)
        Me.KCVTextBox.MaxLength = 6
        Me.KCVTextBox.Name = "KCVTextBox"
        Me.KCVTextBox.ReadOnly = True
        Me.KCVTextBox.Size = New System.Drawing.Size(52, 20)
        Me.KCVTextBox.TabIndex = 3
        '
        'KCVLabel
        '
        Me.KCVLabel.AutoSize = True
        Me.KCVLabel.Location = New System.Drawing.Point(44, 117)
        Me.KCVLabel.Name = "KCVLabel"
        Me.KCVLabel.Size = New System.Drawing.Size(31, 13)
        Me.KCVLabel.TabIndex = 2
        Me.KCVLabel.Text = "KCV:"
        '
        'ComponentTextBoxL
        '
        Me.ComponentTextBoxL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ComponentTextBoxL.Location = New System.Drawing.Point(82, 41)
        Me.ComponentTextBoxL.MaxLength = 16
        Me.ComponentTextBoxL.Name = "ComponentTextBoxL"
        Me.ComponentTextBoxL.Size = New System.Drawing.Size(130, 20)
        Me.ComponentTextBoxL.TabIndex = 1
        '
        'ComponentLabel
        '
        Me.ComponentLabel.AutoSize = True
        Me.ComponentLabel.Location = New System.Drawing.Point(6, 44)
        Me.ComponentLabel.Name = "ComponentLabel"
        Me.ComponentLabel.Size = New System.Drawing.Size(70, 13)
        Me.ComponentLabel.TabIndex = 0
        Me.ComponentLabel.Text = "Componente:"
        '
        'LoadComponentButton
        '
        Me.LoadComponentButton.Location = New System.Drawing.Point(249, 178)
        Me.LoadComponentButton.Name = "LoadComponentButton"
        Me.LoadComponentButton.Size = New System.Drawing.Size(110, 27)
        Me.LoadComponentButton.TabIndex = 4
        Me.LoadComponentButton.Text = "Cargar Componente"
        Me.LoadComponentButton.UseVisualStyleBackColor = True
        '
        'LoadComponentForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(371, 215)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LoadComponentButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "LoadComponentForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cargar Componente"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LoadComponentButton As System.Windows.Forms.Button
    Friend WithEvents KCVTextBox As System.Windows.Forms.TextBox
    Friend WithEvents KCVLabel As System.Windows.Forms.Label
    Friend WithEvents ComponentTextBoxL As System.Windows.Forms.TextBox
    Friend WithEvents ComponentLabel As System.Windows.Forms.Label
    Friend WithEvents FormatLabel As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComponentTextBoxR As System.Windows.Forms.TextBox
End Class
