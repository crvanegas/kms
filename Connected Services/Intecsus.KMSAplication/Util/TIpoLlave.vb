﻿''' <summary>
''' Nomenclatura de los tipo de llaves 
''' </summary>
''' <remarks></remarks>
Public Class TipoLlave
    Structure TipoLlave
        Const Maestra = "MASTER"
        Const Transporte = "TRANSP"
        Const PIN = "PIN"
        Const Datos = "DATA"
        Const PruebaPIN = "TPIN"
        Const PruebaDatos = "TDATA"
    End Structure
End Class
