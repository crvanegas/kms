﻿Imports System.Text
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Environment
Public Class FachadaKMS

    <DllImport("IntecsusKMSNShield.dll",
            BestFitMapping:=True,
            PreserveSig:=True,
            EntryPoint:="KCV_ZMK", SetLastError:=False,
            CharSet:=CharSet.Ansi, ExactSpelling:=False,
            CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function KCV_ZMK(ByVal tokenname As String, ByVal passphrase As String, ByVal zmklabel As String, ByVal KCV As StringBuilder) As Integer
    End Function


    <DllImport("IntecsusKMSNShield.dll",
            BestFitMapping:=True,
            PreserveSig:=True,
            EntryPoint:="GENERATE_KEY", SetLastError:=False,
            CharSet:=CharSet.Ansi, ExactSpelling:=False,
            CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function GENERATE_KEY(ByVal C1 As StringBuilder, ByVal C2 As StringBuilder, ByVal C3 As StringBuilder, ByVal tokenname As String, ByVal passphrase As String) As Integer
    End Function


    <DllImport("IntecsusKMSNShield.dll",
            BestFitMapping:=True,
            PreserveSig:=True,
            EntryPoint:="LOAD_ZMK", SetLastError:=False,
            CharSet:=CharSet.Ansi, ExactSpelling:=False,
            CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function LOAD_ZMK(ByVal C1 As String, ByVal C2 As String, ByVal C3 As String, ByVal tokenname As String, ByVal passphrase As String, ByVal zmklabel As String) As Integer
    End Function

    Public Function kcvZmk(ByVal tokenname As String, ByVal passphrase As String, ByVal zmklabel As String, ByVal kcv As StringBuilder) As Integer
        Return KCV_ZMK(tokenname, passphrase, zmklabel, kcv)
    End Function

      

    Public Function loadZmk(ByVal C1 As String, ByVal C2 As String, ByVal C3 As String, ByVal tokenname As String, ByVal passphrase As String, ByVal key_name As String) As Integer
        Return LOAD_ZMK(C1, C2, C3, tokenname, passphrase, key_name)
    End Function


    Public Function generateKey(ByVal C1 As StringBuilder, ByVal C2 As StringBuilder, ByVal C3 As StringBuilder, ByVal tokenname As String, ByVal passphrase As String) As Integer
        Return GENERATE_KEY(C1, C2, C3, tokenname, passphrase)
    End Function

        


End Class
