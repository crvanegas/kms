﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KMS.Interfaces
{
   public interface IConfig
    {
        /// <summary>
        /// Metodo encargado de obtener el valor asignado en el config.ini
        /// </summary>
        /// <param name="NomArreglo">Nombre del arreglo donde se quiere buscar el valor</param>
        /// <param name="Id">identificador del valor a buscar</param>
        /// <param name="Tamaño">tamaño del valor</param>
        /// <param name="RutaConfig">ruta en la cual se encuentra el config.ini</param>
        /// <returns>string con el valor del identificador</returns>
        /// <remarks>JGCB 18/04/2018</remarks>
         string ObtenerValConfig(string NomArreglo, string Id, int Tamaño, string RutaConfig);

        /// <summary>
        /// Metodo encargado de Escribir un valor en el config.ini
        /// </summary>
        /// <param name="NomArreglo">Nombre del arreglo en el config</param>
        /// <param name="Id">nombre del valor a configurar</param>
        /// <param name="Valor">Valor a ingresar</param>
        /// <param name="RutaConfig">ruta del archivo config.ini</param>
        /// <returns>valor entero (Exito=0, Fallo=1)</returns>
        /// <remarks>JGCB 18/04/2018</remarks>
         Int32 EscribirValConfig(string NomArreglo, string Id, string Valor, string RutaConfig);

    }
}
