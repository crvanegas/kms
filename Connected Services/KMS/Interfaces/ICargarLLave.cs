﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMS.Interfaces
{
    public interface ICargarLLave
    {
        string GenerarLLave(List<string> Componente,Boolean TDES=true);
    }
}
