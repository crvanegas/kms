﻿using Intecsus.Criptografia.Implementacion;
using Intecsus.Criptografia.Interface;
using KMS.Entidad;
using KMS.Interfaces;
using System;
using Intecsus.Seguridad;

namespace KMS.Implementacion
{
   public class LoginKMS: Config, ILogin
    {
        ICriptografia m_Crip = new Cripto();
        bool ILogin.Login(string Usuario, string Password, string RutaConfig, string NomArreglo, string Id1, string Id2, int Tamaño)
        {
            try
            {
                ICriptografia m_Crip = new Cripto();
                OpeBasic Ope = new OpeBasic();
                Ope.ValidarEmpty(Usuario);
                Ope.ValidarEmpty(Password);
                string user = ObtenerValConfig(NomArreglo, Id1, Tamaño, RutaConfig);
                string pw = ObtenerValConfig(NomArreglo, Id2, Tamaño, RutaConfig);
                if (Usuario!= user)
                {
                       throw new Exception("Validando Usuario ");
                }

                else if (m_Crip.SHA512(Password)!= pw)
                {
                    throw new Exception("Validando Contraseña ");
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Login: Error " + ex.Message);
            }
        }
      
        #region "Validar Usuario"
        /// <summary>
        /// Metodo encargado de verificar 
        /// el usuario y contraseña ingresados sean iguales a los almacnados en el config
        /// que la nueva contraseña cumpla con las validaciones de la Expresion Regular
        /// </summary>
        /// <param name="user">usuario actual</param>
        /// <param name="pw">contraseña actual</param>
        /// <param name="Newpw1">nueva contraseña</param>
        /// <param name="Newpw2">verificacion nueva contraseña</param>
        /// <param name="RutaArchivo">ruta donde se encuentra el archivo config.ini</param>
        /// <param name="ExpRegular">Exprecion Regular con validaciones que se desean realizar</param>
        /// <returns>boleano true = Exitoso </returns>
        /// <remarks>JGCB 24/04/2018</remarks>
        public bool ValidarUsuario(string user, string pw, string Newpw1, string Newpw2, string RutaArchivo, string ExpRegular)
        {
            try
            {
                if (user == ObtenerValConfig("login", "user", 512, RutaArchivo) &&
                  m_Crip.SHA512(pw) == ObtenerValConfig("login", "pw", 512, RutaArchivo))
                {
                    if (Newpw1 == Newpw2)
                    {
                        Validadores Val = new Validadores();
                        if (!Val.ValidadorExpresionesRegulares(Newpw1, ExpRegular))
                        {
                            throw new Exception("La contraseña nueva debe tener minimo: \n* 8 caracteres, \n* Una mayúscula \n* Una minúscula");
                        }
                        else
                        {
                            EscribirValConfig("login", "pw", m_Crip.SHA512(Newpw1), RutaArchivo);
                            return true;
                        }
                    }
                    else
                    {
                        throw new Exception("Verificar la nueva contraseña");
                    }
                }
                else
                {
                    throw new Exception("Validar usuario o contraseña anterior");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

    }

}
