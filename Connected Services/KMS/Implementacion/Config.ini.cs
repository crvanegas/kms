﻿using KMS.Entidad;
using KMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KMS.Implementacion
{
    public class Config: IConfig
    {
        #region "Config.ini"

        /// <summary>
        /// Metodo encargado de obtener el valor asignado en el config.ini
        /// </summary>
        /// <param name="NomArreglo">Nombre del arreglo donde se quiere buscar el valor</param>
        /// <param name="Id">identificador del valor a buscar</param>
        /// <param name="Tamaño">tamaño del valor</param>
        /// <param name="RutaConfig">ruta en la cual se encuentra el config.ini</param>
        /// <returns>string con el valor del identificador</returns>
        /// <remarks>JGCB 18/04/2018</remarks>
        public string ObtenerValConfig(string NomArreglo, string Id, int Tamaño, string RutaConfig)
        {
            
            string RutaArchivo = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + RutaConfig;
            StringBuilder Value = new StringBuilder(Tamaño);
            GetPrivateProfileString(NomArreglo, Id, string.Empty, Value, Value.Capacity, RutaArchivo);
            if (Value.ToString() != string.Empty)
            {
                return Value.ToString();
            }
            else
            {
                throw new Exception("Verifique que el archivo en la ruta " + RutaArchivo);
            }
        }

        /// <summary>
        /// Metodo encargado de Escribir un valor en el config.ini
        /// </summary>
        /// <param name="NomArreglo">Nombre del arreglo en el config</param>
        /// <param name="Id">nombre del valor a configurar</param>
        /// <param name="Valor">Valor a ingresar</param>
        /// <param name="RutaConfig">ruta del archivo config.ini</param>
        /// <returns>valor entero (Exito=0, Fallo=1)</returns>
        /// <remarks>JGCB 18/04/2018</remarks>
        public Int32 EscribirValConfig(string NomArreglo, string Id, string Valor, string RutaConfig)
        {
            try
            {
                OpeBasic Ope = new OpeBasic();
                Ope.ValidarEmpty(Valor);
                string RutaArchivo = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + RutaConfig;
                return WritePrivateProfileString(NomArreglo, Id, Valor, RutaArchivo);
            }
            catch(Exception ex)
            {
                throw new Exception("No se permiten campos Vacios o verifique que la ruta del Archivo" + ex.Message);
            }
           
        }

        /// <summary>
        /// Metodo encargado de Extraer los valores seleccionados del un Config.ini
        /// </summary>
        /// <param name="Section">Arreglo de varibles a Seleccionar</param>
        /// <param name="Key">Varialbe a seleccionar </param>
        /// <param name="DefaultValue">Variable por defecto</param>
        /// <param name="Value">StringBuilder donde almacenar el valor</param>
        /// <param name="Size">Capacida del String Builder</param>
        /// <param name="FilePath">Localizacion del Archivo del config.ini</param>
        /// <returns>Entero si encontro el archivo (si =1, no =0)</returns>
        /// <remarks>JGCB 18/14/2018</remarks>
        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern int GetPrivateProfileString(string Seleccion, string Key, string DefaultValue, StringBuilder Value, int Size, string FilePath);

        /// <summary>
        /// Metodo encargado de Ingresar los valore en el config.ini
        /// </summary>
        /// <param name="Seleccion">Arreglo de varibles a Seleccionar</param>
        /// <param name="lpKeyName">Varialbe a seleccionar </param>
        /// <param name="lpString">Valor a ingresar</param>
        /// <param name="lpFileName">Localizacion del Archivo del config.ini</param>
        /// <returns>Entero si encontro el archivo (si =1, no =0)</returns>
        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        private static extern int WritePrivateProfileString(string Seleccion, string lpKeyName, string lpString, string lpFileName);

        #endregion

    }
}
