﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMS.Entidad
{
   public class OpeBasic
    {
        /// <summary>
        ///  Metodo encargado de validar que el campo no se encuentre vacios 
        /// </summary>
        /// <param name="Validate1">valor 1 a validar</param>
        /// <remarks>JGCB 17/04/2018</remarks>      
        /// <retuns>Una Exepcion "No se admiten campos vacios" </retuns>
        public void ValidarEmpty(string Validate1)
        {
            if ((Validate1 == string.Empty))
            {
                throw new Exception("No se admiten campos vacios");
            }
        }


    }
}
