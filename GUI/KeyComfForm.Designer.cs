﻿namespace IntecsusKMS
{
    partial class KeyComfForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyComfForm));
            this.KeyGroupBox = new System.Windows.Forms.GroupBox();
            this.TokenNameTextBox = new System.Windows.Forms.TextBox();
            this.TokenNameLabel = new System.Windows.Forms.Label();
            this.KeyNameTextBox = new System.Windows.Forms.TextBox();
            this.KeyNameLabel = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.KeyGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // KeyGroupBox
            // 
            this.KeyGroupBox.Controls.Add(this.TokenNameTextBox);
            this.KeyGroupBox.Controls.Add(this.TokenNameLabel);
            this.KeyGroupBox.Controls.Add(this.KeyNameTextBox);
            this.KeyGroupBox.Controls.Add(this.KeyNameLabel);
            this.KeyGroupBox.Location = new System.Drawing.Point(12, 12);
            this.KeyGroupBox.Name = "KeyGroupBox";
            this.KeyGroupBox.Size = new System.Drawing.Size(323, 92);
            this.KeyGroupBox.TabIndex = 19;
            this.KeyGroupBox.TabStop = false;
            this.KeyGroupBox.Text = "Información de llaves";
            // 
            // TokenNameTextBox
            // 
            this.TokenNameTextBox.Location = new System.Drawing.Point(125, 55);
            this.TokenNameTextBox.Name = "TokenNameTextBox";
            this.TokenNameTextBox.Size = new System.Drawing.Size(192, 20);
            this.TokenNameTextBox.TabIndex = 14;
            // 
            // TokenNameLabel
            // 
            this.TokenNameLabel.AutoSize = true;
            this.TokenNameLabel.Location = new System.Drawing.Point(38, 62);
            this.TokenNameLabel.Name = "TokenNameLabel";
            this.TokenNameLabel.Size = new System.Drawing.Size(77, 13);
            this.TokenNameLabel.TabIndex = 13;
            this.TokenNameLabel.Text = "Nombre token:";
            // 
            // KeyNameTextBox
            // 
            this.KeyNameTextBox.Location = new System.Drawing.Point(125, 29);
            this.KeyNameTextBox.Name = "KeyNameTextBox";
            this.KeyNameTextBox.Size = new System.Drawing.Size(192, 20);
            this.KeyNameTextBox.TabIndex = 12;
            // 
            // KeyNameLabel
            // 
            this.KeyNameLabel.AutoSize = true;
            this.KeyNameLabel.Location = new System.Drawing.Point(43, 32);
            this.KeyNameLabel.Name = "KeyNameLabel";
            this.KeyNameLabel.Size = new System.Drawing.Size(72, 13);
            this.KeyNameLabel.TabIndex = 11;
            this.KeyNameLabel.Text = "Nombre llave:";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(222, 110);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(113, 26);
            this.SaveButton.TabIndex = 20;
            this.SaveButton.Text = "Guardar";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // KeyComfForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 146);
            this.Controls.Add(this.KeyGroupBox);
            this.Controls.Add(this.SaveButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KeyComfForm";
            this.Text = "KeyComfForm";
            this.KeyGroupBox.ResumeLayout(false);
            this.KeyGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox KeyGroupBox;
        internal System.Windows.Forms.TextBox TokenNameTextBox;
        internal System.Windows.Forms.Label TokenNameLabel;
        internal System.Windows.Forms.TextBox KeyNameTextBox;
        internal System.Windows.Forms.Label KeyNameLabel;
        internal System.Windows.Forms.Button SaveButton;
    }
}