﻿using Intecsus.Seguridad;
using IntecsusKMS.Entidad;
using KMS.Interfaces;
using KMS.Implementacion;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntecsusKMS
{
    public partial class UserConfForm : Form
    {
        public UserConfForm()
        {
            InitializeComponent();
        }
        Componentes Comp = new Componentes();
        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
               ILogin Login = new LoginKMS();
               if(Login.ValidarUsuario(UserTextBox.Text, PwTextBox.Text, Pw1TextBox.Text, Pw2TextBox.Text,Comp.RutaArchivo, @"^(?=(?:.*\d){1})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\S{8,}$"))
                {
                    this.Close();
                    MessageBox.Show("Contraseña Actualizada", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                Logger.Write("Ocurrió un error. Descripción:" + ex.Message, "ExceptionHandling");
                MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
