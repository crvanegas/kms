﻿namespace IntecsusKMS
{
    partial class KeyTypeForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyTypeForm));
            this.KeyNameTextBox = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.cmbComponentes = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.gbTipoLlave = new System.Windows.Forms.GroupBox();
            this.rbPOffSet = new System.Windows.Forms.RadioButton();
            this.rbTransporte = new System.Windows.Forms.RadioButton();
            this.rbMasterK = new System.Windows.Forms.RadioButton();
            this.cmbCargarLlave = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbTipoLlave.SuspendLayout();
            this.SuspendLayout();
            // 
            // KeyNameTextBox
            // 
            this.KeyNameTextBox.Location = new System.Drawing.Point(83, 15);
            this.KeyNameTextBox.Name = "KeyNameTextBox";
            this.KeyNameTextBox.Size = new System.Drawing.Size(139, 20);
            this.KeyNameTextBox.TabIndex = 11;
            this.KeyNameTextBox.TextChanged += new System.EventHandler(this.cmbComponentes_SelectedIndexChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(14, 18);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(69, 13);
            this.Label2.TabIndex = 10;
            this.Label2.Text = "Nombre llave";
            // 
            // cmbComponentes
            // 
            this.cmbComponentes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComponentes.FormattingEnabled = true;
            this.cmbComponentes.Items.AddRange(new object[] {
            "2",
            "3"});
            this.cmbComponentes.SelectedIndex = 0;
            this.cmbComponentes.Location = new System.Drawing.Point(147, 149);
            this.cmbComponentes.Name = "cmbComponentes";
            this.cmbComponentes.Size = new System.Drawing.Size(75, 21);
            this.cmbComponentes.TabIndex = 9;
            this.cmbComponentes.SelectedIndexChanged += new System.EventHandler(this.cmbComponentes_SelectedIndexChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(13, 152);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(132, 13);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "Número de componentes :";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.Location = new System.Drawing.Point(147, 179);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 7;
            this.btnNext.Text = "Siguiente";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // gbTipoLlave
            // 
            this.gbTipoLlave.Controls.Add(this.rbPOffSet);
            this.gbTipoLlave.Controls.Add(this.rbTransporte);
            this.gbTipoLlave.Controls.Add(this.rbMasterK);
            this.gbTipoLlave.Location = new System.Drawing.Point(12, 43);
            this.gbTipoLlave.Name = "gbTipoLlave";
            this.gbTipoLlave.Size = new System.Drawing.Size(210, 70);
            this.gbTipoLlave.TabIndex = 6;
            this.gbTipoLlave.TabStop = false;
            this.gbTipoLlave.Text = "Tipo Llave";
            // 
            // rbPOffSet
            // 
            this.rbPOffSet.AutoSize = true;
            this.rbPOffSet.Location = new System.Drawing.Point(133, 30);
            this.rbPOffSet.Name = "rbPOffSet";
            this.rbPOffSet.Size = new System.Drawing.Size(74, 17);
            this.rbPOffSet.TabIndex = 6;
            this.rbPOffSet.Text = "PIN Offset";
            this.rbPOffSet.UseVisualStyleBackColor = true;
            this.rbPOffSet.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            this.rbPOffSet.Click += new System.EventHandler(this.cmbComponentes_SelectedIndexChanged);
            // 
            // rbTransporte
            // 
            this.rbTransporte.AutoSize = true;
            this.rbTransporte.Location = new System.Drawing.Point(73, 30);
            this.rbTransporte.Name = "rbTransporte";
            this.rbTransporte.Size = new System.Drawing.Size(57, 17);
            this.rbTransporte.TabIndex = 1;
            this.rbTransporte.Text = "Sesión";
            this.rbTransporte.UseVisualStyleBackColor = true;
            this.rbTransporte.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            this.rbTransporte.Click += new System.EventHandler(this.cmbComponentes_SelectedIndexChanged);
            // 
            // rbMasterK
            // 
            this.rbMasterK.AutoSize = true;
            this.rbMasterK.Location = new System.Drawing.Point(4, 30);
            this.rbMasterK.Name = "rbMasterK";
            this.rbMasterK.Size = new System.Drawing.Size(63, 17);
            this.rbMasterK.TabIndex = 0;
            this.rbMasterK.Text = "Maestra";
            this.rbMasterK.UseVisualStyleBackColor = true;
            this.rbMasterK.CheckedChanged += new System.EventHandler(this.RadioButtons_CheckedChanged);
            this.rbMasterK.Click += new System.EventHandler(this.cmbComponentes_SelectedIndexChanged);
            // 
            // cmbCargarLlave
            // 
            this.cmbCargarLlave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCargarLlave.FormattingEnabled = true;
            this.cmbCargarLlave.Items.AddRange(new object[] {
            "Base de Datos"});
            this.cmbCargarLlave.SelectedIndex = 0;
            this.cmbCargarLlave.Location = new System.Drawing.Point(147, 122);
            this.cmbCargarLlave.Name = "cmbCargarLlave";
            this.cmbCargarLlave.Size = new System.Drawing.Size(75, 21);
            this.cmbCargarLlave.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Cargar Llave en :";
            // 
            // KeyTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 209);
            this.Controls.Add(this.cmbCargarLlave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.KeyNameTextBox);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.cmbComponentes);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.gbTipoLlave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KeyTypeForm";
            this.Text = "Configuración Llave";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KeyTypeForm_FormClosing);
            this.gbTipoLlave.ResumeLayout(false);
            this.gbTipoLlave.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox KeyNameTextBox;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox cmbComponentes;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnNext;
        internal System.Windows.Forms.GroupBox gbTipoLlave;
        internal System.Windows.Forms.RadioButton rbTransporte;
        internal System.Windows.Forms.RadioButton rbMasterK;
        internal System.Windows.Forms.RadioButton rbPOffSet;
        internal System.Windows.Forms.ComboBox cmbCargarLlave;
        internal System.Windows.Forms.Label label3;
    }
}

