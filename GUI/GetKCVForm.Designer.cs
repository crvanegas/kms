﻿namespace IntecsusKMS
{
    partial class GetKCVForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GetKCVButton = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.KeyNameTextBox = new System.Windows.Forms.TextBox();
            this.KeyNameLabel = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GetKCVButton
            // 
            this.GetKCVButton.Enabled = false;
            this.GetKCVButton.Location = new System.Drawing.Point(330, 103);
            this.GetKCVButton.Name = "GetKCVButton";
            this.GetKCVButton.Size = new System.Drawing.Size(136, 28);
            this.GetKCVButton.TabIndex = 3;
            this.GetKCVButton.Text = "Consultar KCV";
            this.GetKCVButton.UseVisualStyleBackColor = true;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.KeyNameTextBox);
            this.GroupBox1.Controls.Add(this.KeyNameLabel);
            this.GroupBox1.Location = new System.Drawing.Point(12, 14);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(454, 83);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Información de la llave";
            // 
            // KeyNameTextBox
            // 
            this.KeyNameTextBox.Location = new System.Drawing.Point(122, 39);
            this.KeyNameTextBox.Name = "KeyNameTextBox";
            this.KeyNameTextBox.Size = new System.Drawing.Size(315, 20);
            this.KeyNameTextBox.TabIndex = 1;
            // 
            // KeyNameLabel
            // 
            this.KeyNameLabel.AutoSize = true;
            this.KeyNameLabel.Location = new System.Drawing.Point(18, 42);
            this.KeyNameLabel.Name = "KeyNameLabel";
            this.KeyNameLabel.Size = new System.Drawing.Size(98, 13);
            this.KeyNameLabel.TabIndex = 0;
            this.KeyNameLabel.Text = "Nombre de la llave:";
            // 
            // GetKCVForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 143);
            this.Controls.Add(this.GetKCVButton);
            this.Controls.Add(this.GroupBox1);
            this.Name = "GetKCVForm";
            this.Text = "GetKCVForm";
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button GetKCVButton;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.TextBox KeyNameTextBox;
        internal System.Windows.Forms.Label KeyNameLabel;
    }
}