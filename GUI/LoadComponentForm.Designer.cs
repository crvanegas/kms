﻿namespace IntecsusKMS
{
    partial class LoadComponentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadComponentForm));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.FormatLabel = new System.Windows.Forms.Label();
            this.KCVTextBox = new System.Windows.Forms.TextBox();
            this.KCVLabel = new System.Windows.Forms.Label();
            this.ComponentTextBox = new System.Windows.Forms.TextBox();
            this.ComponentLabel = new System.Windows.Forms.Label();
            this.LoadComponentButton = new System.Windows.Forms.Button();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.FormatLabel);
            this.GroupBox1.Controls.Add(this.KCVTextBox);
            this.GroupBox1.Controls.Add(this.KCVLabel);
            this.GroupBox1.Controls.Add(this.ComponentTextBox);
            this.GroupBox1.Controls.Add(this.ComponentLabel);
            this.GroupBox1.Location = new System.Drawing.Point(12, 12);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(335, 119);
            this.GroupBox1.TabIndex = 5;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Componente";
            // 
            // FormatLabel
            // 
            this.FormatLabel.AutoSize = true;
            this.FormatLabel.ForeColor = System.Drawing.Color.Red;
            this.FormatLabel.Location = new System.Drawing.Point(79, 66);
            this.FormatLabel.Name = "FormatLabel";
            this.FormatLabel.Size = new System.Drawing.Size(0, 13);
            this.FormatLabel.TabIndex = 5;
            // 
            // KCVTextBox
            // 
            this.KCVTextBox.Location = new System.Drawing.Point(82, 84);
            this.KCVTextBox.MaxLength = 6;
            this.KCVTextBox.Name = "KCVTextBox";
            this.KCVTextBox.ReadOnly = true;
            this.KCVTextBox.Size = new System.Drawing.Size(52, 20);
            this.KCVTextBox.TabIndex = 3;
            // 
            // KCVLabel
            // 
            this.KCVLabel.AutoSize = true;
            this.KCVLabel.Location = new System.Drawing.Point(45, 87);
            this.KCVLabel.Name = "KCVLabel";
            this.KCVLabel.Size = new System.Drawing.Size(31, 13);
            this.KCVLabel.TabIndex = 2;
            this.KCVLabel.Text = "KCV:";
            // 
            // ComponentTextBox
            // 
            this.ComponentTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ComponentTextBox.Location = new System.Drawing.Point(82, 41);
            this.ComponentTextBox.MaxLength = 32;
            this.ComponentTextBox.Name = "ComponentTextBox";
            this.ComponentTextBox.Size = new System.Drawing.Size(240, 20);
            this.ComponentTextBox.TabIndex = 1;
            this.ComponentTextBox.TextChanged += new System.EventHandler(this.ComponentTextBox_TextChanged);
            // 
            // ComponentLabel
            // 
            this.ComponentLabel.AutoSize = true;
            this.ComponentLabel.Location = new System.Drawing.Point(6, 44);
            this.ComponentLabel.Name = "ComponentLabel";
            this.ComponentLabel.Size = new System.Drawing.Size(70, 13);
            this.ComponentLabel.TabIndex = 0;
            this.ComponentLabel.Text = "Componente:";
            // 
            // LoadComponentButton
            // 
            this.LoadComponentButton.Location = new System.Drawing.Point(237, 137);
            this.LoadComponentButton.Name = "LoadComponentButton";
            this.LoadComponentButton.Size = new System.Drawing.Size(110, 27);
            this.LoadComponentButton.TabIndex = 6;
            this.LoadComponentButton.Text = "Cargar Componente";
            this.LoadComponentButton.UseVisualStyleBackColor = true;
            this.LoadComponentButton.Click += new System.EventHandler(this.LoadComponentButton_Click);
            // 
            // LoadComponentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 173);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.LoadComponentButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadComponentForm";
            this.Text = "Cargar Componente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadComponentForm_FormClosing);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label FormatLabel;
        internal System.Windows.Forms.TextBox KCVTextBox;
        internal System.Windows.Forms.Label KCVLabel;
        internal System.Windows.Forms.TextBox ComponentTextBox;
        internal System.Windows.Forms.Label ComponentLabel;
        internal System.Windows.Forms.Button LoadComponentButton;
    }
}