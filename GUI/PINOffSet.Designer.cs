﻿namespace IntecsusKMS
{
    partial class PINOffSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PINOffSet));
            this.TBoxTabDec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SelcInicio = new System.Windows.Forms.ComboBox();
            this.SelcLongPin = new System.Windows.Forms.ComboBox();
            this.SelcPadeo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SelcLong = new System.Windows.Forms.ComboBox();
            this.btnCargar = new System.Windows.Forms.Button();
            this.TextBoxTablaDec = new System.Windows.Forms.GroupBox();
            this.LabelForm = new System.Windows.Forms.Label();
            this.TextBoxTablaDec.SuspendLayout();
            this.SuspendLayout();
            // 
            // TBoxTabDec
            // 
            this.TBoxTabDec.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TBoxTabDec.Location = new System.Drawing.Point(142, 18);
            this.TBoxTabDec.MaxLength = 16;
            this.TBoxTabDec.Name = "TBoxTabDec";
            this.TBoxTabDec.Size = new System.Drawing.Size(119, 20);
            this.TBoxTabDec.TabIndex = 6;
            this.TBoxTabDec.TextChanged += new System.EventHandler(this.TBoxTabDec_TextChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tabla Decimalización :";
            // 
            // SelcInicio
            // 
            this.SelcInicio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelcInicio.FormattingEnabled = true;
            this.SelcInicio.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16"});
            this.SelcInicio.Location = new System.Drawing.Point(99, 63);
            this.SelcInicio.Name = "SelcInicio";
            this.SelcInicio.Size = new System.Drawing.Size(41, 21);
            this.SelcInicio.TabIndex = 2;
            this.SelcInicio.SelectedIndex = 0;
            this.SelcInicio.SelectedIndexChanged += new System.EventHandler(this.TBoxTabDec_TextChanged_1);
            // 
            // SelcLongPin
            // 
            this.SelcLongPin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelcLongPin.FormattingEnabled = true;
            this.SelcLongPin.Items.AddRange(new object[] {
            "4"});
            this.SelcLongPin.SelectedIndex = 0;
            this.SelcLongPin.Location = new System.Drawing.Point(99, 144);
            this.SelcLongPin.Name = "SelcLongPin";
            this.SelcLongPin.Size = new System.Drawing.Size(41, 21);
            this.SelcLongPin.TabIndex = 5;
            this.SelcLongPin.SelectedIndexChanged += new System.EventHandler(this.TBoxTabDec_TextChanged_1);
            // 
            // SelcPadeo
            // 
            this.SelcPadeo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelcPadeo.FormattingEnabled = true;
            this.SelcPadeo.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.SelcPadeo.SelectedIndex = 15;
            this.SelcPadeo.Location = new System.Drawing.Point(99, 117);
            this.SelcPadeo.Name = "SelcPadeo";
            this.SelcPadeo.Size = new System.Drawing.Size(41, 21);
            this.SelcPadeo.TabIndex = 4;
            this.SelcPadeo.SelectedIndexChanged += new System.EventHandler(this.TBoxTabDec_TextChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Inicio :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Longitud :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Padeo :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Longitud PIN :";
            // 
            // SelcLong
            // 
            this.SelcLong.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelcLong.FormattingEnabled = true;
            this.SelcLong.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16"});
            this.SelcLong.SelectedIndex = 15;
            this.SelcLong.Location = new System.Drawing.Point(99, 90);
            this.SelcLong.Name = "SelcLong";
            this.SelcLong.Size = new System.Drawing.Size(41, 21);
            this.SelcLong.TabIndex = 10;
            this.SelcLong.SelectedIndexChanged += new System.EventHandler(this.TBoxTabDec_TextChanged_1);
            // 
            // btnCargar
            // 
            this.btnCargar.Location = new System.Drawing.Point(204, 210);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(75, 23);
            this.btnCargar.TabIndex = 11;
            this.btnCargar.Text = "Cargar";
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // TextBoxTablaDec
            // 
            this.TextBoxTablaDec.Controls.Add(this.LabelForm);
            this.TextBoxTablaDec.Controls.Add(this.SelcLongPin);
            this.TextBoxTablaDec.Controls.Add(this.TBoxTabDec);
            this.TextBoxTablaDec.Controls.Add(this.SelcInicio);
            this.TextBoxTablaDec.Controls.Add(this.SelcLong);
            this.TextBoxTablaDec.Controls.Add(this.SelcPadeo);
            this.TextBoxTablaDec.Controls.Add(this.label5);
            this.TextBoxTablaDec.Controls.Add(this.label2);
            this.TextBoxTablaDec.Controls.Add(this.label4);
            this.TextBoxTablaDec.Controls.Add(this.label3);
            this.TextBoxTablaDec.Location = new System.Drawing.Point(12, 12);
            this.TextBoxTablaDec.Name = "TextBoxTablaDec";
            this.TextBoxTablaDec.Size = new System.Drawing.Size(267, 192);
            this.TextBoxTablaDec.TabIndex = 12;
            this.TextBoxTablaDec.TabStop = false;
            this.TextBoxTablaDec.Text = "Parámetros";
            // 
            // LabelForm
            // 
            this.LabelForm.AutoSize = true;
            this.LabelForm.ForeColor = System.Drawing.Color.Red;
            this.LabelForm.Location = new System.Drawing.Point(50, 42);
            this.LabelForm.Name = "LabelForm";
            this.LabelForm.Size = new System.Drawing.Size(0, 13);
            this.LabelForm.TabIndex = 0;
            // 
            // PINOffSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 245);
            this.Controls.Add(this.btnCargar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxTablaDec);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PINOffSet";
            this.Text = "Carga Parámetros PINOFFSET";
            this.TextBoxTablaDec.ResumeLayout(false);
            this.TextBoxTablaDec.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBoxTabDec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox SelcInicio;
        private System.Windows.Forms.ComboBox SelcLongPin;
        private System.Windows.Forms.ComboBox SelcPadeo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox SelcLong;
        private System.Windows.Forms.Button btnCargar;
        internal System.Windows.Forms.GroupBox TextBoxTablaDec;
        private System.Windows.Forms.Label LabelForm;
    }
}