﻿namespace IntecsusKMS
{
    partial class LoadKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadKeyForm));
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.C3Button = new System.Windows.Forms.Button();
            this.LoadKeyButton = new System.Windows.Forms.Button();
            this.C2Button = new System.Windows.Forms.Button();
            this.C1Button = new System.Windows.Forms.Button();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.C3Button);
            this.GroupBox1.Controls.Add(this.LoadKeyButton);
            this.GroupBox1.Controls.Add(this.C2Button);
            this.GroupBox1.Controls.Add(this.C1Button);
            this.GroupBox1.Location = new System.Drawing.Point(11, 10);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(320, 271);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Creación de Llave";
            // 
            // C3Button
            // 
            this.C3Button.Enabled = false;
            this.C3Button.Location = new System.Drawing.Point(88, 141);
            this.C3Button.Name = "C3Button";
            this.C3Button.Size = new System.Drawing.Size(149, 53);
            this.C3Button.TabIndex = 2;
            this.C3Button.Text = "Cargar Componente 3";
            this.C3Button.UseVisualStyleBackColor = true;
            this.C3Button.Click += new System.EventHandler(this.C3Button_Click);
            // 
            // LoadKeyButton
            // 
            this.LoadKeyButton.Enabled = false;
            this.LoadKeyButton.Location = new System.Drawing.Point(88, 201);
            this.LoadKeyButton.Name = "LoadKeyButton";
            this.LoadKeyButton.Size = new System.Drawing.Size(149, 53);
            this.LoadKeyButton.TabIndex = 3;
            this.LoadKeyButton.Text = "Cargar Llave";
            this.LoadKeyButton.UseVisualStyleBackColor = true;
            this.LoadKeyButton.Click += new System.EventHandler(this.LoadKeyButton_Click);
            // 
            // C2Button
            // 
            this.C2Button.Enabled = false;
            this.C2Button.Location = new System.Drawing.Point(88, 82);
            this.C2Button.Name = "C2Button";
            this.C2Button.Size = new System.Drawing.Size(149, 53);
            this.C2Button.TabIndex = 1;
            this.C2Button.Text = "Cargar Componente 2";
            this.C2Button.UseVisualStyleBackColor = true;
            this.C2Button.Click += new System.EventHandler(this.C2Button_Click);
            // 
            // C1Button
            // 
            this.C1Button.Location = new System.Drawing.Point(88, 23);
            this.C1Button.Name = "C1Button";
            this.C1Button.Size = new System.Drawing.Size(149, 53);
            this.C1Button.TabIndex = 0;
            this.C1Button.Text = "Cargar Componente 1";
            this.C1Button.UseVisualStyleBackColor = true;
            this.C1Button.Click += new System.EventHandler(this.C1Button_Click);
            // 
            // LoadKeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 289);
            this.Controls.Add(this.GroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadKeyForm";
            this.Text = "Crear LLave";
            this.GroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button C3Button;
        internal System.Windows.Forms.Button LoadKeyButton;
        internal System.Windows.Forms.Button C2Button;
        internal System.Windows.Forms.Button C1Button;
    }
}