﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using IntecsusKMS.Entidad;
using IntecsusKMS.ServicioKMS;
using KMS.Implementacion;
using KMS.Interfaces;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace IntecsusKMS
{
    public partial class KeyTypeForm : Form
    {
        #region "Variables"
        TipoLlave Tl = new TipoLlave();
        IConfig m_Ctg = new Config();
        Componentes Comp;
        Operaciones Ope = new Operaciones();
        string NomSelec = string.Empty;

        #endregion

        public KeyTypeForm(Componentes C)
        {
            Comp = C;
            InitializeComponent();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                m_Ctg.EscribirValConfig("key", "key_name", KeyNameTextBox.Text, Comp.RutaArchivo);
                Comp.CantidadComponentes = cmbComponentes.SelectedItem.ToString();
                Form form = new LoadKeyForm(Comp);
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Visible = true;
                this.Hide();
            }
            catch (Exception ex)
            {
                Logger.Write("Ocurrió un error. Descripción:" + ex.Message, "ExceptionHandling");
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void RadioButtons_CheckedChanged(object sender, EventArgs e)  // Handles rbMasterK.CheckedChanged, rbTransporte.CheckedChanged, rbPIN.CheckedChanged, rbDatos.CheckedChanged, rbTestPin.CheckedChanged, rbTestData.CheckedChanged
        {
            try
            {
                NomSelec = Ope.ObtenerChecked(rbMasterK, rbTransporte) +
                Ope.ObtenerChecked(rbPOffSet, rbPOffSet);

                    Comp.CargarEn = NomSelec.Remove(NomSelec.IndexOf("."));
                    string NomConfig = m_Ctg.ObtenerValConfig("key", "key_name", 512, Comp.RutaArchivo);

                    if (NomConfig.Contains(NomSelec))
                    {
                        KeyNameTextBox.Text = NomSelec + NomConfig.Substring(NomConfig.IndexOf('.') + 1);
                    }
                    else
                    {
                        KeyNameTextBox.Text = NomSelec + KeyNameTextBox.Text.Substring(KeyNameTextBox.Text.IndexOf('.') + 1);
                    }                
            }
            catch (Exception ex)
            {
                Logger.Write("Ocurrió un error. Descripción:" + ex.Message, "ExceptionHandling");
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmbComponentes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((cmbComponentes.SelectedItem.ToString() != "0") &&
                (NomSelec != "0") && (cmbCargarLlave.SelectedItem.ToString() != "0")&&
                KeyNameTextBox.Text != string.Empty)
            {
                btnNext.Enabled = true;
            }
            else
            {
                btnNext.Enabled = false;
            }
        }

        private void KeyTypeForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

    }
}
