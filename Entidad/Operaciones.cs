﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Intecsus.Criptografia.Implementacion;
using Intecsus.Criptografia.Interface;
using Intecsus.Seguridad;
using IntecsusKMS.Entidad;

namespace IntecsusKMS.Entidad
{
    public class Operaciones
    {
        #region "Implementacion Componentes"
        /// <summary>
        /// Metodo encargado de Verificar que se ingresen valores Hexadecimales
        /// </summary>
        /// <param name="Text">Texto Ingresado por el usuario</param>
        /// <returns>Mensaje segun el caso</returns>
        public string VerifiCaracteres(string Text, string rangoCarateres)
        {
            if (!Regex.Match(Text, "^[" + rangoCarateres + "]*$", RegexOptions.IgnoreCase).Success)
            {
                return "Usar sólo caracteres de:" + rangoCarateres;
            }
            return string.Empty;
        }



        #endregion

        #region "Metodos propios"
        /// <summary>
        /// Metodo encargado de obtener el boton seleccionado de una lista 
        /// </summary>
        /// <param name="Buttons">Lista de botones</param>
        /// <returns>String con boton seleccionado</returns>
        public string ObtenerChecked(RadioButton Id1, RadioButton Id2)
        {
            List<RadioButton> Buttons = new List<RadioButton>();
            Buttons.Add(Id1); Buttons.Add(Id2);
            foreach (var Button in Buttons)
            {
                if (Button.Checked)
                {
                    TipoLlave Tl = new TipoLlave();
                    return Tl.TLlave[Button.Name] + ".";
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Metodo encargado de Validar la conexion con la base de datos
        /// </summary>
        /// <param name="resp">respuesta de conexion</param>
        public void BaseDatos(string resp)
        {
            try
            {
                if (resp == "00")
                {
                    MessageBox.Show("Carga Exitosa", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    throw new Exception("Error con la Base de Datos");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Error con la Base de Datos" + exc.Message);
            }
        }
        #endregion
    }
}
