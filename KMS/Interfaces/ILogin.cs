﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMS.Interfaces
{
  public interface ILogin
    {
        /// <summary>
        /// Metodo encargado de Validar si las variables ingresadas son iguales
        /// a las almacenadas en el Config.ini
        /// </summary>
        /// <param name="Usuario">Usuario Ingresado</param>
        /// <param name="Password">Password Ingresado</param>
        /// <param name="RutaConfig">Ruta del archivo Config.ini</param>
        /// <param name="NomArreglo">Nombre del arreglo en el Config.ini</param>
        /// <param name="Valor1">Nombre de la Variable en el arreglo</param>
        /// <param name="Valor2">Nombre de la Variable en el arreglo</param>
        /// <param name="Tamaño">Tamaño de las Variables (8, 256, 512, 1024...)</param>
        /// <returns>true o false segun el caso</returns>
        /// <remarks>JGCB 18/04/2018</remarks>
       bool Login(string Usuario, string Password, string RutaConfig, string NomArreglo, string Id1, string Id2, int Tamaño);

        /// <summary>
        /// Metodo encargado de verificar 
        /// el usuario y contraseña ingresados sean iguales a los almacnados en el config
        /// que la nueva contraseña cumpla con las validaciones de la Expresion Regular
        /// </summary>
        /// <param name="user">usuario actual</param>
        /// <param name="pw">contraseña actual</param>
        /// <param name="Newpw1">nueva contraseña</param>
        /// <param name="Newpw2">verificacion nueva contraseña</param>
        /// <param name="RutaArchivo">ruta donde se encuentra el archivo config.ini</param>
        /// <param name="ExpRegular">Exprecion Regular con validaciones que se desean realizar</param>
        /// <returns>boleano true = Exitoso </returns>
        /// <remarks>JGCB 24/04/2018</remarks>
        bool ValidarUsuario(string user, string pw, string Newpw1, string Newpw2, string RutaArchivo, string ExpRegular);
    }
}
