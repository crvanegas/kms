﻿using Intecsus.Criptografia.Implementacion;
using Intecsus.Criptografia.Interface;
using KMS.Entidad;
using KMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMS.Implementacion
{
    public class CargarLlave : Config, ICargarLLave
    {

        public string GenerarLLave(List<string> Componente, Boolean TDES=true)
        {
            ICriptografia m_Crip = new Cripto();
            string Key = string.Empty;
            string KCV = string.Empty;
            try
            {
                if (String.IsNullOrEmpty(Componente[1]) || Componente.Count() == 0)
                {
                    throw new Exception("No Cargo ningun Componente o solo cargo uno \n" +
                        "Por Favor Vuelva a cargarlos");
                }
                else
                {
                    Key = m_Crip.XOR(Componente[0], Componente[1]);
                    KCV = m_Crip.KCV(Key, TDES);

                    if (Componente.Count() == 3)
                    {
                        Key = m_Crip.XOR(Key, Componente[2]);
                        KCV = m_Crip.KCV(Key, TDES);
                    }
                    MessageBox.Show("El KCV de la llave es: " + KCV, "KCV", MessageBoxButtons.OK,
                             MessageBoxIcon.Information);
                    return Key;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "La clave especificada es una clave débil conocida para 'TripleDES' y no se puede utilizar.")
                {
                    DialogResult dialogResult = MessageBox.Show("La clave especificada es débil." + "\n ¿Esta seguro de usarla?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (dialogResult == DialogResult.Yes)
                    {
                        return  GenerarLLave(Componente, false);
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                       throw new Exception("Vueva a comenzar el proceso de carga"+ex.Message);
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }
                }
                else
                {
                      throw new Exception(ex.Message);
                }               
            }
        }
    }
}
